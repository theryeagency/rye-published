<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Rye Published
 *
 * @package     ExpressionEngine
 * @category    Plugin
 * @author      The Rye Agency
 * @copyright   Copyright (c) 2016, The Rye Agency
 * @link        https://rye.agency/
 */

class Rye_published
{
    public $return_data = "";

    public function __construct()
    {
        $this->EE =& get_instance();

        // Grab the parameter for the channels we're checking against
        $channel_id = ee()->TMPL->fetch_param('channel_id');

        // Grab the current member ID
        $current_user = $this->EE->session->userdata('member_id');

        $results = ee()->db->select('*')
                            ->from('channel_titles c')
                            ->where(array(
                              'c.author_id' => $current_user,
                              'c.channel_id' => $channel_id
                            ))
                            ->get();

        if ($results->num_rows() > 0)
        {
          $this->return_data = "true";
        } else {
          $this->return_data = "false";
        }

    }

}
