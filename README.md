Rye Published
============

An ExpressionEngine plugin that allows you to check if the current user has published an entry in a given channel
for ExpressionEngine 2 and ExpressionEngine 3.


# Parameters
- **channel_id**: A single channel ID (no piped values in this version) for the channels you want to check against

# Returns
- **true**: if the member has published an entry in your channel
- **false**: if they have not

# Example
{exp:rye_published channel_id="4"}

# Upgrading from Version 1
Rename `{exp:rc_published}` to `{exp:rye_published}`
